DEBUG_FLAGS = -Wall -Wextra -Wmismatched-dealloc -Wmismatched-new-delete -Wfree-nonheap-object
SRC_FILES = src/main.cpp src/functions.cpp
LIBRARIES = -I include/

all: release

run: release
	bin/release/work

clean:
	rm -f bin/debug/* bin/release/*

release: bin/release/ $(SRC_FILES)
	g++ -o bin/release/work $(SRC_FILES) $(LIBRARIES)

debug: bin/debug/ $(SRC_FILES)
	g++ -g -o bin/debug/work $(SRC_FILES) $(DEBUG_FLAGS) $(LIBRARIES)

bin/release/:
	mkdir -p bin/release/

bin/debug/:
	mkdir -p bin/debug/