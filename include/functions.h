#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <cstring> // strlen
#include <iomanip>
#include <math.h>

typedef double (*Fun)(double);

//данные для печати результатов интегрирования
struct I_print{
    const char* name;// название функции
    double i_sum;    // значение интегральной суммы
    double i_toch;   // точное значение интеграла
    int n;           // число разбиений области
};

void printTable(I_print i_prn[],int k);

double fun1(double x);
double fun2(double x);
double fun3(double x);
double fun4(double x);

// precise integrals of functions
double integral1(double a, double b);
double integral2(double a, double b);
double integral3(double a, double b);
double integral4(double a, double b);

// integral
double intRect(Fun fun, double a, double b, double precision, int& count);
double intTrap(Fun fun, double a, double b, double precision, int& count);

#endif