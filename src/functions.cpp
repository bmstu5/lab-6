#include "functions.h"

// what the hell I can do with multicharacters? How to print them?
#define C_196 '-'
#define C_218 '+'
#define C_179 '|'
#define C_195 '+'
#define C_194 '+'
#define C_191 '+'
#define C_197 '+'
#define C_180 '+'
#define C_192 '+'
#define C_193 '+'
#define C_217 '+'

void printTable(I_print i_prn[],int k)
{
	const int   m = 4;                 //число столбцов
	const int   wn[m] = {12,18,18,10}; //ширина столбцов
	const char* titles[m]={"Function","Integral","IntSum","N "};
	
	int sizes[m]; // sizes of titles
	for(int i=0; i<m; i++)
		sizes[i] = std::strlen(titles[i]);

	// first string
	std::cout << C_218 << std::setfill(C_196);
	for(int j=0; j<m-1; j++)
		std::cout << std::setw(wn[j]) << C_194;
	std::cout << std::setw(wn[m-1]) << C_191 << '\n';
	
	// titles
	std::cout << C_179;
	for(int j=0; j<m; j++)
	{	std::cout << std::setw((wn[j]-sizes[j])/2) << std::setfill(' ') << ' ';
		std::cout << titles[j];
		std::cout << std::setw((wn[j]-sizes[j])/2) << C_179;
	}
	std::cout << '\n';

	// table
	for(int i=0; i<k; i++)
	{	// string
		std::cout << C_195 << std::fixed;
		for(int j=0; j<m-1; j++)
			std::cout << std::setfill(C_196) << std::setw(wn[j]) << C_197;
		std::cout << std::setw(wn[m-1]) << C_180 << std::setfill(' ') << '\n';

		// function name
		std::cout << C_179 << std::setw((wn[0]-strlen(i_prn[i].name))/2) << ' ';
		std::cout << i_prn[i].name;
		std::cout << std::setw((wn[0] - strlen(i_prn[i].name))/2 + strlen(i_prn[i].name)%2) << C_179;

		// results
		std::cout << std::setw(wn[1]-1) << std::setprecision(10) << i_prn[i].i_toch << C_179;
		std::cout << std::setw(wn[2]-1) << i_prn[i].i_sum << std::setprecision(6) << C_179;
		std::cout << std::setw(wn[3]-1) << i_prn[i].n << C_179 << '\n';
	}
	
	 // last string
	std::cout << C_192 << std::setfill(C_196);
	for(int j=0; j<m-1; j++)
		std::cout << std::setw(wn[j]) << C_193;
	std::cout << std::setw(wn[m-1]) << C_217 << std::setfill(' ') << '\n';
}

double fun1(double x)
{	return x;
}
double fun2(double x)
{	return std::sin(x * 22);
}
double fun3(double x)
{	return std::pow(x, 4);
}
double fun4(double x)
{	return std::atan(x);
}
double integral1(double a, double b)
{	return (b*b - a*a) / 2;
}
double integral2(double a, double b)
{	return (std::cos(a*22) - std::cos(b*22)) / 22;
}
double integral3(double a, double b)
{	return (b*b*b*b*b - a*a*a*a*a) / 5;
}
double integral4(double a, double b)
{	return b*atan(b) - a*atan(a) - (log(b*b+1) - log(a*a+1)) / 2;
}

double intRect(Fun fun, double a, double b, double precision, int& count)
{
	count = 1;
	double s1 = 0, s2 = 0;

	do {
		if (a > b)
				std::swap(a, b);
		s1 = s2;
		s2 = 0;
		double step = abs(b - a) / (double) count;
		for (int currentN = 1; currentN <= count; currentN++) {
			s2 += fun(a + currentN * step - step/2);
		}
		s2 *= step;
		count *= 2;
	} while (abs(s2-s1) > precision);
	count /= 2;

	return s2;
}

double intTrap(Fun fun, double a, double b, double precision, int& count)
{
	count = 1;
	double s1 = 0, s2 = 0;

	do {
		if (a > b)
				std::swap(a, b);
		s1 = s2;
		s2 = 0;
		double step = abs(b - a) / (double) count;
		for (int currentN = 1; currentN <= count; currentN++) {
			s2 += fun(a + (currentN-1) * step) + fun(a + currentN * step);
		}
		s2 *= step/2;
		count *= 2;
	} while (abs(s2-s1) > precision);
	count /= 2;

	return s2;
}