#include <iostream>
#include "functions.h"

int main()
{
	double llimit = -1;
	double rlimit = 3;
	double (*intsum)(Fun, double, double, double, int&) = intRect; // or intTrap

	std::cout << std::setw(62) << std::setfill('/') << '/' << '\n';
	std::cout << "Быстродействие алгортима в зависимости от функции и требуемой\n";
	std::cout << "точности (быстродействие характеризуется числом отрезков n[i])\n";
	std::cout << std::setw(62) << std::setfill('/') << '/' << '\n';
	std::cout << "Область определения функций: " << llimit << " <= x <= " << rlimit << '\n';

	// print 5 tables
	for (double precision = 0.01; precision >= 0.000001; precision /= 10)
	{	I_print to_print[4];

		to_print[0].i_sum = intsum(fun1, llimit, rlimit, precision, to_print[0].n);
		to_print[0].i_toch = integral1(llimit, rlimit);
		to_print[0].name = "y=x";

		to_print[1].i_sum = intsum(fun2, llimit, rlimit, precision, to_print[1].n);
		to_print[1].i_toch = integral2(llimit, rlimit);
		to_print[1].name = "y=sin(22x)";
		
		to_print[2].i_sum = intsum(fun3, llimit, rlimit, precision, to_print[2].n);
		to_print[2].i_toch = integral3(llimit, rlimit);
		to_print[2].name = "y=x^4";
		
		to_print[3].i_sum = intsum(fun4, llimit, rlimit, precision, to_print[3].n);
		to_print[3].i_toch = integral4(llimit, rlimit);
		to_print[3].name = "y=arctg(x)";

		std::cout << "        Точность вычислений: " << precision << '\n';
		printTable(to_print, 4);

		std::cout << '\n';
	}

	return 0;
}